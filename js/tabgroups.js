/* The tabgroups.js file handles all background events surrounding 
 * the creation, renaming, and management of tab groups. This includes 
 * saving tab group configurations, adding in new tab information and 
 * thumbnails. The tabgroups.js and tabs.js event scripts will work 
 * closely with one another. 
 */ 
 /* 
Renaming created groups should be implemented here. 
 */ 

//var lastGroup; //To store the lastly used group when the user closed the browser last time.
var currentGroup = ""; //Instantly stores the currently-in-use tab-group.
var lockStorage = false;    // When true, local storage does not store tab group data

initialize();

function initialize(clearStorage=false){
    // Use for emergencies!!!
    if(clearStorage == true) {
        chrome.storage.sync.clear();
        chrome.storage.sync.set({"tabGroups":{"tabgroup1":{}}});
    }
    
    if("currentGroup" in chrome.storage.sync){
        chrome.storage.sync.get("currentGroup", function (groupName) {
            console.log(groupName);
            currentGroup = groupName;
            openGroup(currentGroup);
        });
    }else{
        console.log("No currentGroup exists.");
        currentGroup = "tabgroup1";
        chrome.storage.sync.set({'currentGroup': currentGroup});
        getOpenTabs(function(tabArray){
            formGroup(currentGroup, tabArray, function(){});
        });
    }
}

function formGroup(groupName, tabArray, callback) 
{ 
    let tabGroup = {}; 
    chrome.storage.sync.get(null, function(temp2){
        if('tabGroups' in temp2){
            chrome.storage.sync.get('tabGroups', function(temp){
                tabGroup = temp;
                tabGroup['tabGroups'][groupName] = {}; 
            //    console.log(tabArray.length);
                for(let i = 0; i < tabArray.length; i++) { 
                    let tab = tabArray[i]; 
                    tabGroup['tabGroups'][groupName][tab.title] = [tab.url, tab.pinned]; 
                } 
                console.log(tabGroup);
                callback(tabGroup); 
            });
        }else{
            console.log("Clearing tabGroups.")
            tabGroup['tabGroups'] = {};
             tabGroup['tabGroups'][groupName] = {}; 
        //    console.log(tabArray.length);
            for(let i = 0; i < tabArray.length; i++) { 
                let tab = tabArray[i]; 
                tabGroup['tabGroups'][groupName][tab.title] = [tab.url, tab.pinned]; 
            }
        } 
        console.log(tabGroup);
        callback(tabGroup); 
    });
} 
 
 
function saveGroup(tabGroup) 
{
    if(!lockStorage) {
        chrome.storage.sync.set(tabGroup, function() {
            // Notify that we saved
            console.log('Settings saved');
        });
    }
}

//This function returns the currently-in-use tab-group.
function getCurrentGroup(){
    console.log(currentGroup);
    return currentGroup;
}

function loadGroup(groupName, callback){
    // Allow for getting all group data
    if(groupName == "*") {
        chrome.storage.sync.get('tabGroups', function(result){
            //console.log(result);
            let tabgroups = result['tabGroups'];
            console.log(tabgroups);
            callback(tabgroups);
        });
    }
    // Otherwise, simply return one group
    chrome.storage.sync.get('tabGroups', function(result){
        //console.log(result);
        let tabs = result['tabGroups'][groupName];
        console.log(tabs);
        callback(tabs);
    });
}

function getGroupNames(callback) {
    chrome.storage.sync.get('tabGroups', function(allGroups) {
        let groupNames = [];
        for(let groupName in allGroups["tabGroups"]) {
            groupNames.push(groupName);
        }
        callback(groupNames);
    });
}

//This function would leave one blank page.
function closeAllTabs(callback){
    lockStorage = false; // Stop local storage from being updated
    chrome.tabs.create({'url': "http://www.google.com"},function(tab){
        chrome.tabs.query({
            'currentWindow': true
        }, function (tabs) {
            lockStorage = true;
            for (let i in tabs) {
                if(tabs[i].id!=tab.id){
                    chrome.tabs.remove(tabs[i].id);
                }
            }
            lockStorage = false; 
            callback();
        });
    });
}

function openGroup(groupName){
//    lockStorage = true; // Stop local storage from being updated
    closeAllTabs(function(){
        chrome.storage.sync.get('tabGroups', function(result){
            let tabs = result["tabGroups"][groupName];
//            lockStorage = false;    // Re-enable storage updates
            for (index in tabs){
                chrome.tabs.create({url:tabs[index].url}, function(){});
            }
            currentGroup = groupName;
            chrome.storage.sync.set({currentGroup: currentGroup});
        });
    });
    if(arguments.length < 1){
        return;
    }else{
        currentGroup = groupName;
        chrome.storage.sync.set({'currentGroup': currentGroup}, function(){
            closeAllTabs(function(){
                chrome.storage.sync.get('tabGroups', function(result){
                    let tabs = result["tabGroups"][groupName];
                    for (index in tabs){
                        chrome.tabs.create({url:tabs[index].url}, function(){});
                    }
                });
            });
        });
    }
}

var dncount = 1;
function defaultNameCounter(){
    dncount += 1;
    return dncount;
}

function generateDefaultName(callback){
    str = "tabgroup";
//    str += defaultNameCounter();
    getGroupNames(function(groupNames){
        console.log(groupNames.length+1);
        str += groupNames.length+1;
        callback(str);
    });
}

function createGroup(groupName = ""){
    if(arguments.length < 1){
        console.log("Creating group name.")
        generateDefaultName(function(str){
            groupName = str;
            currentGroup = groupName;
            chrome.storage.sync.set({'currentGroup': currentGroup}, function(){
                chrome.storage.sync.get('tabGroups', function(result){
                    console.log(result);
                    result['tabGroups'][groupName] = {}; 
                    closeAllTabs(function(){ });
                });
//            chrome.tabs.create({},function(){});
//            lockStorage = true; // Stop local storage from being updated
                });
            });
        }else{
//        lockStorage = true; // Stop local storage from being updated
        currentGroup = groupName;
        chrome.storage.sync.set({'currentGroup': currentGroup}, function(){
            chrome.storage.sync.get('tabGroups', function(result){
                result['tabGroups'][groupName] = {}; 
                closeAllTabs(function(){ });
//            chrome.tabs.create({},function(){});
//            lockStorage = true; // Stop local storage from being updated
            });
        });
    }
}

function removeGroup(groupName, callback) {
    if(groupName == currentGroup) // Don't want to delete the current one!
        return;
    chrome.storage.sync.get("tabGroups", function(tabGroups) {
            removedGroup = tabGroups["tabGroups"][groupName];
            delete tabGroups["tabGroups"][groupName];
            chrome.storage.sync.set(tabGroups, function() {
                callback(removedGroup); // Return tab group data to callback for group-renaming functionality
            });
    });
}