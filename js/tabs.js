// Get the current tabs open in the Google Chrome browser window
function getOpenTabs(callback){
    var tabArray = [];
    chrome.tabs.query(
        {
            'currentWindow': true
        }, 
        function(result){
            tabArray = result;
            callback(tabArray);
        }
    );
}


/*function UpdateStorage(tabGroupName, tabGroupData)
{
    var tabGroupDictionary = {};
    tabGroupDictionary[tabGroupName] = tabGroupData;
    // Save data in a customized JSON format
    chrome.storage.sync.set(tabGroupDictionary, function() {
        // Notify that we saved
        //message('Settings saved');
    });
}*/


// We can use this to monitor the data stored (in bytes) by
// using this function. Strictly for debugging purposes only.
//StorageArea.getBytesInUse(null, function(numBytes) {
//    console.log(numBytes);
//})


// Check when local storage is updated
chrome.storage.onChanged.addListener(function(changes, namespace) {
    for (key in changes) {
        var storageChange = changes[key];
        console.log("Storage key \"%s\" in namespace \"%s\" changed. " +
            'Old value was "%s", new value is "%s".',
            key,
            namespace,
            storageChange.oldValue,
            storageChange.newValue);
    }
//    loadGroup("tabgroup1", function(tabs){});
    loadGroup(getCurrentGroup(), function(tabs){});
});

function updateGroup(callback){
    getOpenTabs(function(temp){
        callback(temp); 
    });
}

//triggered when a tab is created or opened.
chrome.tabs.onCreated.addListener(function(tab){
    console.log("tabs.onCreated"); 
    if(!lockStorage) {
        updateGroup(function(tabArray){ 
//           formGroup("tabgroup1", tabArray, function(tabGroup){
            formGroup(getCurrentGroup(), tabArray, function(tabGroup){
                saveGroup(tabGroup)
            });
        });
    }
});

//triggered when a tab is updated.
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, updatedTab) {
    console.log("tabs.onUpdated: ");
    console.log(changeInfo);
    if("status" in changeInfo && changeInfo["status"]=="complete"){ //Temperory solution only.
        
    } else if(!lockStorage) {
        updateGroup(function(tabArray) {
            formGroup(getCurrentGroup(), tabArray, function(tabGroup) {
                saveGroup(tabGroup)
            });
        });
    }
//    alert(updatedTab.url);
});     

//Fired when a tab is closed.
/**
/* @tabId: Integer
/* @removeInfo : Object {integer windowId: <windowId>, boolean isWindowClosing: <isWindowClosing>}
*/
chrome.tabs.onRemoved.addListener(function(tabId, removeInfo){
    console.log("tabs.onRemoved"); 
    if(!lockStorage) {
        updateGroup(function(tabArray){ 
            formGroup(getCurrentGroup(), tabArray, function(tabGroup){
                saveGroup(tabGroup);
            });
        });
    }
});



