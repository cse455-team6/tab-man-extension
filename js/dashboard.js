/**********************************************************************
 *                        Tab Group Functions
 **********************************************************************/
var backgroundPage = chrome.extension.getBackgroundPage();
console.log(backgroundPage);


function renderTabGroup(groupName) {
	let str = "";
	backgroundPage.loadGroup(groupName, function(tabGroup){
		for(let key in tabGroup){
			tab = tabGroup[key];
			let title = key.substring(0,50);
			str += '<nav class="tabs-nav navbar navbar-expand-xlg">';
            //console.log(title);
            str += '<a href="'+ tab[0] +'" target="_blank"><h5 id="#tabUrl">'+ title + '</h5></a>';
            str += '<a  href="#" class="btn" role="button" aria-pressed="true" onlick="">';
            str += '<i class="fa fa-times" aria-hidden="true"></i></a></nav>';
		}
		//console.log(str);
		insertIntoDiv(str);
	});
	
}

function insertIntoDiv(x) {
	let tabURL = $("#tabs");
    // Clear contents before appending
    //tabURL.remove("nav");
    $("nav.tabs-nav").remove();
    //tabURL.innerHTML = "";
	tabURL.append(x);
	
}

renderTabGroup(backgroundPage.currentGroup);

function renderGroupNames(groupNames){
	let str ="";
	for(let i = 0; i < groupNames.length; i++) {
		name = groupNames[i];
		str +='<nav class="tab-group-name navbar navbar-expand-xlg">';
        str +='<a href="#" class="btn btn-primary" role="button" aria-pressed="true">';
        str +='<i class="fa fa-lock" aria-hidden="true"></i></a><h4 class="tab-group-name"  >' + name + '</h4>';
        str +='<a  href="#" class="btn btn-primary" role="button" aria-pressed="true">';
        str +='<i class="fa fa-times" aria-hidden="true"></i></a></nav>';           
	}
	insertGroupNames(str);
};


function insertGroupNames(x) {
	let tabGroups = $("#tab_groups");
	$(tabGroups).append(x);	
}


backgroundPage.getGroupNames(function (groupNames) {
    renderGroupNames(groupNames);
});



/**********************************************************************
 *                           NAV Functions
 **********************************************************************/

function openNav() {
    document.getElementById("sideNavigation").style.width = "250px";
}
 
function closeNav() {
    document.getElementById("sideNavigation").style.width = "0";
}

// Hides sideNavigation when user clicks on body
$( document.body ).click(function() {
    closeNav();
});

// Allows reopening of sideNavigation after it is initially hidden
$( "#openNav" ).click(function(e) {
    e.stopPropagation();    // Stops the event from bubbling up to the body
    openNav();
});

// Don't close the side nav when user clicks on it
$( "#sideNavigation" ).click(function(e) {
    e.stopPropagation();    // Stops the event from bubbling up to the body
});

// Close the sideNavigation by clicking the "X" button
$( "#closeNav" ).click(function() {
    closeNav();
});	

// Removes all tab group entries. Careful!
$( "#clearStorage" ).click(function() {
    // Clears the local extension storage and starts over
    backgroundPage.initialize(true);
});	

// View tabs of selected group
$( "#tab_groups" ).on("click", "nav.tab-group-name", function() {
    renderTabGroup($(this).text());
});


// Use this method to call background JavaScript functions

backgroundPage.loadGroup("*", function(returnData) {
    console.log(returnData);
});