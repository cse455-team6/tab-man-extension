var bgPage = chrome.extension.getBackgroundPage();

$("#freshGroup").click(function () {
    bgPage.createGroup();
});

$("#webPage_btn").click(function(){
    window.location.href="../webpage.html";
});

/*$(".clickableGroupName").click(function() {
    console.log($(this).text());
    bgPage.openGroup($(this).text());
});*/

// Open tabs of the selected group
$( "#tabGroupList" ).on("click", ".clickableGroupName", function() {
    bgPage.openGroup($(this).text());
});

function renderGroupNames(groupNames){
	let str ="";
	for(let i =0; i < groupNames.length; i++) {
		name = groupNames[i];
        str +='<button type="button" class="btn btn-link btn-block clickableGroupName">' + 
                name + '</button>';
	}
	insertGroupNames(str);
};


function insertGroupNames(x) {
	let tabGroups = $("#tabGroupList");
	$(tabGroups).append(x);	
}


bgPage.getGroupNames(function(groupNames) {
    console.log("groupNames:");
    console.log(groupNames);
    /*for(let groupName in groupNames) {
        renderGroupNames(groupName);
    }*/
    renderGroupNames(groupNames);
    chrome.storage.sync.get(null, function(data) {
        console.log("storage.sync.get all: ");
        console.log(data);
    })
});
//renderGroupNames("tabgroup2");