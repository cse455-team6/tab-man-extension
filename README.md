# Tab Man Chrome Extension

Created from a software engineering project developed by four Computer Science 
and Engineering students at California State University, San Bernardino, 
Tab Man is a real-life superhero who fights against the chaos monster Tabzilla. 
 
You may have encountered Tabzilla before when it started feeding on your 
computer's RAM supply. Never again will you have to endure such hardship for 
Tab Man is here to save the day!